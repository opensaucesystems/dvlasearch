<?php
namespace opensaucesystems\dvlasearch\endpoints {

    class tyres extends baseEndpoint {

        use \opensaucesystems\dvlasearch\service\http;
        use \opensaucesystems\dvlasearch\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Tyres
         *
         * Usage:
         *   $dvlasearch->tyres->find($licencePlate)
         *
         * Result:
            stdClass Object
            (
                [make] => VOLKSWAGEN
                [model] => TIGUAN SE TDI 4MOTION
                [year] => 2009
                [frontTyres] => Array
                    (
                        [0] => stdClass Object
                            (
                                [width] => 235
                                [ratio] => 55
                                [rim] => R17
                                [speedRating] => H
                                [psi] => 29
                                [loadIndex] => 99
                            )
            
                        [1] => stdClass Object
                            (
                                [width] => 235
                                [ratio] => 50
                                [rim] => R18
                                [speedRating] => H
                                [psi] => 29
                                [loadIndex] => 97
                            )
            
                    )
            
                [rearTyres] => Array
                    (
                        [0] => stdClass Object
                            (
                                [width] => 235
                                [ratio] => 55
                                [rim] => R17
                                [speedRating] => H
                                [psi] => 29
                                [loadIndex] => 99
                            )
            
                        [1] => stdClass Object
                            (
                                [width] => 235
                                [ratio] => 50
                                [rim] => R18
                                [speedRating] => H
                                [psi] => 29
                                [loadIndex] => 97
                            )
            
                    )
            
            )
         *
         * @param string $licencePlate
         * @return object
         */
        public function find($licencePlate = '')
        {
            $this->config->params['licencePlate'] = $licencePlate;
            
            $endpoint = 'TyreSearch';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
