<?php
namespace opensaucesystems\dvlasearch\endpoints {

    class account extends baseEndpoint {

        use \opensaucesystems\dvlasearch\service\http;
        use \opensaucesystems\dvlasearch\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Account
         *
         * Usage:
         *   $dvlasearch->account->info()
         *
         * Result:
            stdClass Object
            (
                [usedCredit] => 284644
                [totalCredit] => 1000000
                [avgDailyUse] => 178
            )
         *
         * @return object
         */
        public function info()
        {
            $endpoint = 'SearchCount';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
