<?php
namespace opensaucesystems\dvlasearch\endpoints {

    class mot extends baseEndpoint {

        use \opensaucesystems\dvlasearch\service\http;
        use \opensaucesystems\dvlasearch\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Mot
         *
         * Usage:
         *   $dvlasearch->mot->find($licencePlate)
         *
         * Result:
            stdClass Object
            (
                [make] => VOLKSWAGEN
                [model] => TIGUAN SE TDI 4MOTION 140
                [dateFirstUsed] => 23 July 2009
                [fuelType] => Diesel
                [colour] => SILVER
                [motTestReports] => Array
                    (
                        [0] => stdClass Object
                            (
                                [testDate] => 29 April 2016
                                [expiryDate] => 28 April 2017
                                [testResult] => Pass
                                [odometerReading] => 80084
                                [motTestNumber] => 244794981138
                                [advisoryItems] => Array
                                    (
                                    )
            
                                [failureItems] => Array
                                    (
                                    )
            
                            )
            
                        [1] => stdClass Object
                            (
                                [testDate] => 22 April 2015
                                [expiryDate] => 21 April 2016
                                [testResult] => Pass
                                [odometerReading] => 74998
                                [motTestNumber] => 938552815158
                                [advisoryItems] => Array
                                    (
                                    )
            
                                [failureItems] => Array
                                    (
                                    )
            
                            )
            
                        [2] => stdClass Object
                            (
                                [testDate] => 25 March 2015
                                [expiryDate] => 
                                [testResult] => Fail
                                [odometerReading] => 74813
                                [motTestNumber] => 157384685012
                                [advisoryItems] => Array
                                    (
                                        [0] => Front Brake pad(s) wearing thin (3.5.1g)
                                        [1] => Front brake disc worn, pitted or scored, but not seriously weakened (3.5.1i)
                                        [2] => Rear brake disc worn, pitted or scored, but not seriously weakened (3.5.1i)
                                        [3] => Rear Brake pad(s) wearing thin (3.5.1g)
                                        [4] => Nearside Front Suspension arm rubber bush deteriorated but not resulting in excessive movement (2.4.G.2)
                                        [5] => Offside Front Suspension arm rubber bush deteriorated but not resulting in excessive movement (2.4.G.2)
                                    )
            
                                [failureItems] => Array
                                    (
                                        [0] => Nearside Front Tyre tread depth below requirements of 1.6mm (4.1.E.1)
                                        [1] => Offside Front Tyre tread depth below requirements of 1.6mm (4.1.E.1)
                                        [2] => Front Brakes imbalanced across an axle (3.7.B.5b)
                                        [3] => Nearside Front Windscreen wiper does not clear the windscreen effectively (8.2.2)
                                    )
            
                            )
            
                        [3] => stdClass Object
                            (
                                [testDate] => 24 June 2014
                                [expiryDate] => 23 July 2015
                                [testResult] => Pass
                                [odometerReading] => 63643
                                [motTestNumber] => 195455474181
                                [advisoryItems] => Array
                                    (
                                    )
            
                                [failureItems] => Array
                                    (
                                    )
            
                            )
            
                        [4] => stdClass Object
                            (
                                [testDate] => 23 July 2013
                                [expiryDate] => 23 July 2014
                                [testResult] => Pass
                                [odometerReading] => 52191
                                [motTestNumber] => 198414503254
                                [advisoryItems] => Array
                                    (
                                    )
            
                                [failureItems] => Array
                                    (
                                    )
            
                            )
            
                        [5] => stdClass Object
                            (
                                [testDate] => 6 July 2012
                                [expiryDate] => 23 July 2013
                                [testResult] => Pass
                                [odometerReading] => 308600
                                [motTestNumber] => 945398182180
                                [advisoryItems] => Array
                                    (
                                    )
            
                                [failureItems] => Array
                                    (
                                    )
                            )
                    )
            )
         *
         * @param string $licencePlate
         * @return object
         */
        public function find($licencePlate = '')
        {
            $this->config->params['licencePlate'] = $licencePlate;
            
            $endpoint = 'MotHistory';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
