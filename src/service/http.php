<?php
namespace opensaucesystems\dvlasearch\service {

    use \Httpful\Request;

    trait http
    {
        public function get($url)
        {
            return Request::get($url.'?'.http_build_query($this->config->params))
                ->expectsJson()
                ->addHeaders($this->config->headers)
                ->send()->body;
        }

        public function put($url, $payload)
        {
            return Request::put($url)
                ->sendsJson()
                ->authenticateWith(
                    $this->config->user,
                    $this->config->key
                )
                ->body($payload)
                ->send()->body;
        }

        public function post($url, $payload)
        {
            return Request::post($url)
                ->sendsJson()
                ->authenticateWith(
                    $this->config->user,
                    $this->config->key
                )
                ->body($payload)
                ->send()->body;
        }

        public function delete($url, $payload)
        {
            return Request::delete($url)
                ->sendsJson()
                ->authenticateWith(
                    $this->config->user,
                    $this->config->key
                )
                ->body($payload)
                ->send()->body;
        }

        public function head()
        {

        }

        public function options()
        {

        }
    }

}
