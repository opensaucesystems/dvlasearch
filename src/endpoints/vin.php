<?php
namespace opensaucesystems\dvlasearch\endpoints {

    class vin extends baseEndpoint {

        use \opensaucesystems\dvlasearch\service\http;
        use \opensaucesystems\dvlasearch\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Vin
         *
         * Usage:
         *   $dvlasearch->vin->find($vin)
         *
         * Result:
            stdClass Object
            (
                [co2Emissions] => 219 g/km
                [model] => Amg Gt
                [twelveMonthRate] => 
                [transmission] => Automatic
                [vrm] => 10GRX
                [colour] => WHITE
                [sixMonthRate] => 
                [yearOfManufacture] => 2015
                [dateOfFirstRegistration] => 31 July 2015
                [fuelType] => PETROL
                [revenueWeight] => 1890kg
                [typeApproval] => M1
                [vin] => WMX1903782A004387
                [wheelPlan] => 2 AXLE RIGID BODY
                [motDetails] => No details held by DVLA
                [make] => MERCEDES-BENZ
                [taxDetails] => Tax due: 01 August 2016
            )
         *
         * @param string $vin
         * @return object
         */
        public function find($vin = '')
        {
            $this->config->params['vin'] = $vin;
            
            $endpoint = 'VINSearch';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
