<?php
namespace opensaucesystems\dvlasearch {

	use \Exception;

	class APIClient {

		private $config;
		
		/**
		 * 
		 */
		public function __construct($apikey, $config = [])
		{
			$this->config = $config + [
				'baseuri'  => 'https://dvlasearch.appspot.com/',
				'endpoint' => null,
				'params'   => [
					'apikey' => $apikey
				],
				'headers' => [
                    'Accept-Language' => 'en-gb', 
                    'Accept' => 'application/json'
                ]
			];
			$this->config = (object) $this->config;
		}

		public function __get($endpoint)
		{
			$class = __NAMESPACE__.'\\endpoints\\'.$endpoint;

			if (class_exists($class)) {
				$this->config->endpoint = $endpoint;
				return $this->$endpoint = new $class($this->config);
			} else {
				throw new Exception(
					'Endpoint '.$class.', not implemented.'
				);
			}
		}

	}

}
