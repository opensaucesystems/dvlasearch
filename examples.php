<?php
require 'vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');

/**
 * Initialize the API client class
 */
$dvlasearch = new opensaucesystems\dvlasearch\APIClient(
    'DvlaSearchDemoAccount' //
);

/**
    @call: $dvlasearch->vehicle->find('mt09nks')
    
    @response:
    stdClass Object
    (
        [make] => VOLKSWAGEN
        [dateOfFirstRegistration] => 23 July 2009
        [yearOfManufacture] => 2009
        [cylinderCapacity] => 1968 cc
        [co2Emissions] => 167 g/km
        [fuelType] => DIESEL
        [taxStatus] => Tax not due
        [colour] => SILVER
        [typeApproval] => M1
        [wheelPlan] => 2 AXLE RIGID BODY
        [revenueWeight] => Not available
        [taxDetails] => Tax due: 01 April 2017
        [motDetails] => Expires: 28 April 2017
        [taxed] => 1
        [mot] => 1
        [vin] => WVGZZZ5NZAW007903
        [model] => TIGUAN SE TDI 4MOTION 140
        [transmission] => MANUAL
        [numberOfDoors] => 5
        [sixMonthRate] => 
        [twelveMonthRate] => 
    )

 */
//echo '<pre>'.print_r($dvlasearch->vehicle->find('mt09nks'), true).'</pre>';

/**
    @call: $dvlasearch->mot->find('mt09nks')
    
    @response:
    stdClass Object
    (
        [make] => VOLKSWAGEN
        [model] => TIGUAN SE TDI 4MOTION 140
        [dateFirstUsed] => 23 July 2009
        [fuelType] => Diesel
        [colour] => SILVER
        [motTestReports] => Array
            (
                [0] => stdClass Object
                    (
                        [testDate] => 29 April 2016
                        [expiryDate] => 28 April 2017
                        [testResult] => Pass
                        [odometerReading] => 80084
                        [motTestNumber] => 244794981138
                        [advisoryItems] => Array
                            (
                            )
    
                        [failureItems] => Array
                            (
                            )
    
                    )
    
                [1] => stdClass Object
                    (
                        [testDate] => 22 April 2015
                        [expiryDate] => 21 April 2016
                        [testResult] => Pass
                        [odometerReading] => 74998
                        [motTestNumber] => 938552815158
                        [advisoryItems] => Array
                            (
                            )
    
                        [failureItems] => Array
                            (
                            )
    
                    )
    
                [2] => stdClass Object
                    (
                        [testDate] => 25 March 2015
                        [expiryDate] => 
                        [testResult] => Fail
                        [odometerReading] => 74813
                        [motTestNumber] => 157384685012
                        [advisoryItems] => Array
                            (
                                [0] => Front Brake pad(s) wearing thin (3.5.1g)
                                [1] => Front brake disc worn, pitted or scored, but not seriously weakened (3.5.1i)
                                [2] => Rear brake disc worn, pitted or scored, but not seriously weakened (3.5.1i)
                                [3] => Rear Brake pad(s) wearing thin (3.5.1g)
                                [4] => Nearside Front Suspension arm rubber bush deteriorated but not resulting in excessive movement (2.4.G.2)
                                [5] => Offside Front Suspension arm rubber bush deteriorated but not resulting in excessive movement (2.4.G.2)
                            )
    
                        [failureItems] => Array
                            (
                                [0] => Nearside Front Tyre tread depth below requirements of 1.6mm (4.1.E.1)
                                [1] => Offside Front Tyre tread depth below requirements of 1.6mm (4.1.E.1)
                                [2] => Front Brakes imbalanced across an axle (3.7.B.5b)
                                [3] => Nearside Front Windscreen wiper does not clear the windscreen effectively (8.2.2)
                            )
    
                    )
    
                [3] => stdClass Object
                    (
                        [testDate] => 24 June 2014
                        [expiryDate] => 23 July 2015
                        [testResult] => Pass
                        [odometerReading] => 63643
                        [motTestNumber] => 195455474181
                        [advisoryItems] => Array
                            (
                            )
    
                        [failureItems] => Array
                            (
                            )
    
                    )
    
                [4] => stdClass Object
                    (
                        [testDate] => 23 July 2013
                        [expiryDate] => 23 July 2014
                        [testResult] => Pass
                        [odometerReading] => 52191
                        [motTestNumber] => 198414503254
                        [advisoryItems] => Array
                            (
                            )
    
                        [failureItems] => Array
                            (
                            )
    
                    )
    
                [5] => stdClass Object
                    (
                        [testDate] => 6 July 2012
                        [expiryDate] => 23 July 2013
                        [testResult] => Pass
                        [odometerReading] => 308600
                        [motTestNumber] => 945398182180
                        [advisoryItems] => Array
                            (
                            )
    
                        [failureItems] => Array
                            (
                            )
                    )
            )
    )

 */
//echo '<pre>'.print_r($dvlasearch->mot->find('mt09nks'), true).'</pre>';

/**
    @call: $dvlasearch->tyres->find('mt09nks')
    
    @response:
    stdClass Object
    (
        [make] => VOLKSWAGEN
        [model] => TIGUAN SE TDI 4MOTION
        [year] => 2009
        [frontTyres] => Array
            (
                [0] => stdClass Object
                    (
                        [width] => 235
                        [ratio] => 55
                        [rim] => R17
                        [speedRating] => H
                        [psi] => 29
                        [loadIndex] => 99
                    )
    
                [1] => stdClass Object
                    (
                        [width] => 235
                        [ratio] => 50
                        [rim] => R18
                        [speedRating] => H
                        [psi] => 29
                        [loadIndex] => 97
                    )
    
            )
    
        [rearTyres] => Array
            (
                [0] => stdClass Object
                    (
                        [width] => 235
                        [ratio] => 55
                        [rim] => R17
                        [speedRating] => H
                        [psi] => 29
                        [loadIndex] => 99
                    )
    
                [1] => stdClass Object
                    (
                        [width] => 235
                        [ratio] => 50
                        [rim] => R18
                        [speedRating] => H
                        [psi] => 29
                        [loadIndex] => 97
                    )
    
            )
    
    )

 */
//echo '<pre>'.print_r($dvlasearch->tyres->find('mt09nks'), true).'</pre>';

/**
    @call: $dvlasearch->account->info()
    
    @response:
    stdClass Object
    (
        [usedCredit] => 284644
        [totalCredit] => 1000000
        [avgDailyUse] => 178
    )


 */
//echo '<pre>'.print_r($dvlasearch->account->info(), true).'</pre>';

/**
    @call: $dvlasearch->vin->find('WMX1903782A004387')
    
    @response:
    stdClass Object
    (
        [co2Emissions] => 219 g/km
        [model] => Amg Gt
        [twelveMonthRate] => 
        [transmission] => Automatic
        [vrm] => 10GRX
        [colour] => WHITE
        [sixMonthRate] => 
        [yearOfManufacture] => 2015
        [dateOfFirstRegistration] => 31 July 2015
        [fuelType] => PETROL
        [revenueWeight] => 1890kg
        [typeApproval] => M1
        [vin] => WMX1903782A004387
        [wheelPlan] => 2 AXLE RIGID BODY
        [motDetails] => No details held by DVLA
        [make] => MERCEDES-BENZ
        [taxDetails] => Tax due: 01 August 2016
    )

 */
//echo '<pre>'.print_r($dvlasearch->vin->find('WMX1903782A004387'), true).'</pre>';

/**
    @call: $dvlasearch->valuation->find('WMX1903782A004387')
    
    @response:
    stdClass Object
    (
        [model] => Tiguan
        [engineSize] => 1968CC
        [numberOfSeats] => 5
        [transmission] => Manual
        [imported] => false
        [bodyType] => ESTATE
        [insuranceGroup] => 09
        [price] => 6465.0
        [capID] => 38489
        [description] => 2009 Volkswagen Tiguan Se Tdi (140) 4MOTION, 1968CC Diesel, 5DR, Manual
        [steering] => RHD
        [fuel] => Diesel
        [year] => 2009
        [numberOfDoors] => 5
        [make] => Volkswagen
        [capCode] => VWTI20SE5EDTM4
    )

 */
//echo '<pre>'.print_r($dvlasearch->valuation->find('mt09nks'), true).'</pre>';
