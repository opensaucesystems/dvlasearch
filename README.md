![dvlasearch.co.uk - Logo](http://i.imgur.com/PtjMEwk.jpg)

## DVLASearch - API Client

This PHP composer package provides a simple object orientated way to interact with the http://dvlasearch.co.uk API.

**Setup:**

Using composer add the package to your project:

`composer require opensaucesystems/dvlasearch`

**Initializing the client:**

    <?php
    require 'vendor/autoload.php';
        
    /**
     * Initialize the API client class
     */
    $dvlasearch = new opensaucesystems\dvlasearch\APIClient(
        'DvlaSearchDemoAccount' // API key
    );

**Making calls:**

To interact with the API, its as simple as calling a class method or property, endpoints are dynamically loaded when traversed to.

**For example:**

`print_r($dvlasearch->vin->find('WMX1903782A004387'), true);`

    stdClass Object
    (
        [co2Emissions] => 219 g/km
        [model] => Amg Gt
        [twelveMonthRate] => 
        [transmission] => Automatic
        [vrm] => 10GRX
        [colour] => WHITE
        [sixMonthRate] => 
        [yearOfManufacture] => 2015
        [dateOfFirstRegistration] => 31 July 2015
        [fuelType] => PETROL
        [revenueWeight] => 1890kg
        [typeApproval] => M1
        [vin] => WMX1903782A004387
        [wheelPlan] => 2 AXLE RIGID BODY
        [motDetails] => No details held by DVLA
        [make] => MERCEDES-BENZ
        [taxDetails] => Tax due: 01 August 2016
    )

See: `examples.php` for further calls and there results.



