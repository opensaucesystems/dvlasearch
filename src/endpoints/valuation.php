<?php
namespace opensaucesystems\dvlasearch\endpoints {

    class valuation extends baseEndpoint {

        use \opensaucesystems\dvlasearch\service\http;
        use \opensaucesystems\dvlasearch\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Valuation
         *
         * Usage:
         *   $dvlasearch->valuation->find($licencePlate)
         *
         * Result:
            stdClass Object
            (
                [model] => Tiguan
                [engineSize] => 1968CC
                [numberOfSeats] => 5
                [transmission] => Manual
                [imported] => false
                [bodyType] => ESTATE
                [insuranceGroup] => 09
                [price] => 6465.0
                [capID] => 38489
                [description] => 2009 Volkswagen Tiguan Se Tdi (140) 4MOTION, 1968CC Diesel, 5DR, Manual
                [steering] => RHD
                [fuel] => Diesel
                [year] => 2009
                [numberOfDoors] => 5
                [make] => Volkswagen
                [capCode] => VWTI20SE5EDTM4
            )
         *
         * @param string $licencePlate
         * @return object
         */
        public function find($licencePlate = '')
        {
            $this->config->params['licencePlate'] = $licencePlate;
            
            $endpoint = 'Valuation';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
