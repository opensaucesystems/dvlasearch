<?php
namespace opensaucesystems\dvlasearch\endpoints {

    class vehicle extends baseEndpoint {

        use \opensaucesystems\dvlasearch\service\http;
        use \opensaucesystems\dvlasearch\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Vehicle
         *
         * Usage:
         *   $dvlasearch->vehicle->find($licencePlate)
         *
         * Result:
            stdClass Object
            (
                [make] => VOLKSWAGEN
                [dateOfFirstRegistration] => 23 July 2009
                [yearOfManufacture] => 2009
                [cylinderCapacity] => 1968 cc
                [co2Emissions] => 167 g/km
                [fuelType] => DIESEL
                [taxStatus] => Tax not due
                [colour] => SILVER
                [typeApproval] => M1
                [wheelPlan] => 2 AXLE RIGID BODY
                [revenueWeight] => Not available
                [taxDetails] => Tax due: 01 April 2017
                [motDetails] => Expires: 28 April 2017
                [taxed] => 1
                [mot] => 1
                [vin] => WVGZZZ5NZAW007903
                [model] => TIGUAN SE TDI 4MOTION 140
                [transmission] => MANUAL
                [numberOfDoors] => 5
                [sixMonthRate] => 
                [twelveMonthRate] => 
            )
         *
         * @param string $licencePlate
         * @return object
         */
        public function find($licencePlate = '')
        {
            $this->config->params['licencePlate'] = $licencePlate;
            
            $endpoint = 'DvlaSearch';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
